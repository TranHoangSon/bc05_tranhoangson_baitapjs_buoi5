// Start Tra cứu điểm
function tinhDiem() {
  khuVuc();
  doiTuong();

  var diem1 = document.getElementById("txt-diemmon1").value * 1;
  var diem2 = document.getElementById("txt-diemmon2").value * 1;
  var diem3 = document.getElementById("txt-diemmon3").value * 1;
  var diemTong = diem1 + diem2 + diem3 + khuVuc() + doiTuong();
  console.log(diemTong);
  var diemChuan = document.getElementById("txt-diemchuan").value * 1;
  var ketQua = "";
  if (diemChuan > diemTong || diem1 * diem2 * diem3 == 0) {
    ketQua = "RỚT";
  } else {
    ketQua = "ĐẬU";
  }
  document.getElementById(
    "result"
  ).innerHTML = `<p class= "text-success">Tổng Điểm: ${diemTong} </p> <p>Kết Quả: ${ketQua}`;
}
function khuVuc() {
  var e = document.getElementById("inputGroupSelect01");
  var value = e.value;
  var text = e.options[e.selectedIndex].text;
  var diemKhuVuc = 0;
  if (text == "A") {
    diemKhuVuc = 2;
  } else if (text == "B") {
    diemKhuVuc = 1;
  } else if (text == "C") {
    diemKhuVuc = 0.5;
  } else {
    diemKhuVuc = 0;
  }
  return diemKhuVuc;
}
function doiTuong() {
  var a = document.getElementById("inputGroupSelect02");
  var value2 = a.value;
  var text2 = a.options[a.selectedIndex].text;
  var diemDoiTuong = 0;
  if (text2 == 1) {
    diemDoiTuong = 2.5;
  } else if (text2 == 2) {
    diemDoiTuong = 1.5;
  } else if (text2 == 3) {
    diemDoiTuong = 1;
  } else {
    diemDoiTuong = 0;
  }
  return diemDoiTuong;
}
// end tra cứu điểm

// start Tính tiền điện

function tinhTienDien() {
  var number = document.getElementById("txt-soKW").value * 1;
  var Total = 0;
  if (number <= 50) {
    Total = number * 500;
  } else if (number <= 100) {
    +(number - 50) * 650;
  } else if (number <= 200) {
    Total = 50 * 500 + 50 * 650 + (number - 100) * 850;
  } else if (number <= 350) {
    Total = 50 * 500 + 50 * 650 + 100 * 850 + (number - 200) * 1100;
  } else {
    Total =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (number - 350) * 1300;
  }
  var name = document.getElementById("txt-ten").value;
  console.log(name);
  document.getElementById(
    "result2"
  ).innerHTML = `Họ tên: ${name} ; Tiền Điện: ${Total}`;
}
